((nil . ((indent-tabs-mode . nil)
         (tab-width . 2)
         (typescript-indent-level . 2)
         (fill-column . 120))))
