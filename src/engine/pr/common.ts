export const ETYPE = {
  ev_void: 0,
  ev_string: 1,
  ev_float: 2,
  ev_vector: 3,
  ev_entity: 4,
  ev_field: 5,
  ev_function: 6,
  ev_pointer: 7
};

export const opnames = [
  'DONE',
  'MUL_F', 'MUL_V', 'MUL_FV', 'MUL_VF',
  'DIV',
  'ADD_F', 'ADD_V',
  'SUB_F', 'SUB_V',
  'EQ_F', 'EQ_V', 'EQ_S', 'EQ_E', 'EQ_FNC',
  'NE_F', 'NE_V', 'NE_S', 'NE_E', 'NE_FNC',
  'LE', 'GE', 'LT', 'GT',
  'INDIRECT', 'INDIRECT', 'INDIRECT', 'INDIRECT', 'INDIRECT', 'INDIRECT',
  'ADDRESS',
  'STORE_F', 'STORE_V', 'STORE_S', 'STORE_ENT', 'STORE_FLD', 'STORE_FNC',
  'STOREP_F', 'STOREP_V', 'STOREP_S', 'STOREP_ENT', 'STOREP_FLD', 'STOREP_FNC',
  'RETURN',
  'NOT_F', 'NOT_V', 'NOT_S', 'NOT_ENT', 'NOT_FNC',
  'IF', 'IFNOT',
  'CALL0', 'CALL1', 'CALL2', 'CALL3', 'CALL4', 'CALL5', 'CALL6', 'CALL7', 'CALL8',
  'STATE',
  'GOTO',
  'AND', 'OR',
  'BITAND', 'BITOR'
];

export const OP = {
  done: 0,
  mul_f: 1, mul_v: 2, mul_fv: 3, mul_vf: 4,
  div_f: 5,
  add_f: 6, add_v: 7,
  sub_f: 8, sub_v: 9,
  eq_f: 10, eq_v: 11, eq_s: 12, eq_e: 13, eq_fnc: 14,
  ne_f: 15, ne_v: 16, ne_s: 17, ne_e: 18, ne_fnc: 19,
  le: 20, ge: 21, lt: 22, gt: 23,
  load_f: 24, load_v: 25, load_s: 26, load_ent: 27, load_fld: 28, load_fnc: 29,
  address: 30,
  store_f: 31, store_v: 32, store_s: 33, store_ent: 34, store_fld: 35, store_fnc: 36,
  storep_f: 37, storep_v: 38, storep_s: 39, storep_ent: 40, storep_fld: 41, storep_fnc: 42,
  ret: 43,
  not_f: 44, not_v: 45, not_s: 46, not_ent: 47, not_fnc: 48,
  jnz: 49, jz: 50,
  call0: 51, call1: 52, call2: 53, call3: 54, call4: 55, call5: 56, call6: 57, call7: 58, call8: 59,
  state: 60,
  jump: 61,
  and: 62, or: 63,
  bitand: 64, bitor: 65
};
